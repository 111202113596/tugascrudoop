<?php
include "config.php";
?>

<h1>Data User</h1>
<div style="text-align: center;">
    <a href="usertambah.php" style="margin-right:10px;">Tambah Data</a> 
</div>
<table border="1" width="80%" cellpadding="10" style="margin: 10px auto">
    <tr>
        <th width="10px">NO</th>
        <th>Username</th>
        <th>Password</th>
        <th>Peran</th>
        <th colspan="2">Aksi</th>
    </tr>

    <?php
    $result = mysqli_query($koneksi, "SELECT * FROM user");
    $no = 0;
    while ($row = mysqli_fetch_array($result)) :
        $no++;
    ?>
        <tr>
            <td align="center"><?= $no; ?></td>
            <td><?= $row['username']; ?></td>
            <td><?= $row['password']; ?></td>
            <td><?= $row['peran']; ?></td>
            <td><a href="useredit.php?Id=<?= $row['kode_user'] ?>">Edit</a></td>
            <td><a href="funcHapusUser.php?Id=<?= $row['kode_user'] ?>">Hapus</a></td>
        </tr>
    <?php endwhile; ?>

</table>